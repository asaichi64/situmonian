/**
 * @author Asaichi-Note00
 * 
 * RoomManagerなどでもっている、UIの作成関数をこのクラスでまとめ、
 * FlowManagerの登録を自動で行うクラスです。
 * 
 */
var FAG = require( 'fag/FAG' );
function FlowRegister()
{
	var _FUNCLIST = [];
	var dummyfunc = function() {}
	
// -------------------------------------	
	//! 初期化
	this.Init = function()
	{
		//! 登録を行う。
		this.Entry();
	}		
	
	//! 登録
	this.Entry = function()
	{
		var INDEX_ID = 0;
		var INDEX_FUNC = 1;
		//! FUNCLIST flormat: id, func
		//! ルーム関係 -----------------------------------------
		var IFlowManager = require( 'data/FlowManager' ).GetInstance();
		var IRoomManager = require( 'ui/room/RoomManager' ).GetInstance();
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_TOP, IRoomManager.CreateHostRoomTop] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_LOGIN, dummyfunc] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_ROOMCREATE, IRoomManager.CreateHostRoomCreate] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_ROOMMANAGE, IRoomManager.CreateHostRoomManage] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_START, dummyfunc] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_PLAY, dummyfunc] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_OVER, dummyfunc] );
		_FUNCLIST.push( [IFlowManager.FLOW_HOST_RESULT, dummyfunc] );
				
				
		//! フロー登録
		var IFlowManager = require( 'data/FlowManager' ).GetInstance();
		for( var i=0 ; i<_FUNCLIST.length ; i++ )
		{
			var data = _FUNCLIST[ i ];
			var id = data[ INDEX_ID ]  ;
			var func = data[ INDEX_FUNC ];
			
			//FAG.Console.write( "registry id=" + id + " func=" + func );
			//! フロー登録
			IFlowManager.RegistryFlow( id, func );
		}
		
		
		FAG.Console.write( "Entry Successed" );
	}
	
	
}
module.exports = FlowRegister;

//! インスタンスを取得
var Instance = null;
function GetInstance()
{
	if( Instance == null )
	{
		Instance = new FlowRegister();
	}
	return Instance;
}
module.exports.GetInstance = GetInstance; 
