/**
 * @author Asaichi-Note00
 * 
 * ルームのUIを管理する
 * ここではWindowをCreateする関数を用意し、
 * FlowRegisterを介してFlowManagerに登録する事で
 * フロー制御の簡易化を図ってます。
 */

var FAG = require( 'fag/FAG' );
//! ルームの管理、作成関数のサポート
function RoomManager()
{
	//! アプリマネージャ
	var IAppManager = require( 'data/AppManager' ).GetInstance();
	//! フローマネージャ
	var IFlowManager = require('data/FlowManager').GetInstance();
	//! データマネージャ
	var IDataManager = require('data/DataManager').GetInstance();
	
	//! ルーム汎用処理クラス
	var IRoom = require( 'ui/common/Room' ).GetInstance();
	
	
//! ----------------------------------------------
	//! 初期化 
	this.Init = function()
	{
		
	}	
	
	//! FLOW_HOST_TOP 
	this.CreateHostRoomTop = function()
	{
		//! 
		var self = Ti.UI.createWindow({
			title:'ルーム作成',
			backgroundColor:'white',
			layout:'vertical'
		});
		// --------------------------------------------------------	
		
		//! 中央表示物
		var view_center = Ti.UI.createView({
			title:'view_center',
			width:200,
			height:100,
			backgroundColor:'#999',
			layout:'horizontal'
		})
		
		var lab_room = Ti.UI.createLabel( {
			text:"名前を入れてね"
		});
		view_center.add( lab_room );
		//! 入力
		var tf_room = Titanium.UI.createTextField({
		        color:'#336699',
		        width:200,
		        height:40,
		        hintText:'あなたのなまえ',
		        textAlign:'center',
		        keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		        returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		        borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		});
		tf_room.addEventListener( 'change', function(e) {
			//! 名前設定
			IDataManager.SetNickName( e.value );
		});
		view_center.add( tf_room );
		//! ルーム作成ボタン
		var tf_button = Ti.UI.createButton({
			title:"ルーム作成"
		});
		tf_button.addEventListener( 'click', function() {
			//var win = CreateRoomCreateConfigWindow();
			//! 次の画面はルーム作成画面
			var win = IFlowManager.ChangeFlow( IFlowManager.FLOW_HOST_ROOMCREATE );
			IAppManager.OpenCurrentTab( win );
		});
		view_center.add( tf_button );
		
		//! 
		self.add( view_center );
		
		return self;
	}
	
	//! FLOW_HOST_ROOMCREATE: ホストルーム作成画面
	this.CreateHostRoomCreate = function()
	{
		var win = Ti.UI.createWindow({
				title:'ルームホスト画面',
				backgroundColor:'white',
				layout:'vertical'
			})
			
		//! ルーム名設定
		var view_room = Ti.UI.createView({
			title:'view_room',
			width:200,
			height:60,
			layout:'horizontal'
		});
		
		//! テキスト
		var lab_roomname = Ti.UI.createLabel({
			text:'ルーム名を入力してね'
		})
		view_room.add( lab_roomname );
		
		//! 入力
		var tf_room = Titanium.UI.createTextField({
		        color:'#336699',
		        width:200,
		        height:40,
		        hintText:'るーむのなまえ',
		        textAlign:'center',
		        keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
		        returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
		        borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
		});
		tf_room.addEventListener( 'change', function(e){
			//! 名前を設定
			IDataManager.SetRoomName( e.value );
		});
		view_room.add( tf_room );
				
		win.add( view_room );		
		
		//! 質問設定
		var view_ques = Ti.UI.createView({
			title:'view_ques',
			width:300,
			height:100,
			layout:'horizontal'
		});
		
		var lab_ques = Ti.UI.createLabel({
			text:'質問を入力してね'
		})
		view_ques.add( lab_ques );
		//! 入力
		var ta_ques = Ti.UI.createTextArea({
	        value:'',
	        height:70,
	        width:300,
	        font:{fontSize:20,fontFamily:'Marker Felt', fontWeight:'bold'},
	        color:'#888',
	        textAlign:'left',
	        appearance:Titanium.UI.KEYBOARD_APPEARANCE_ALERT,       
	        keyboardType:Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION,
	        returnKeyType:Titanium.UI.RETURNKEY_EMERGENCY_CALL,
	        borderWidth:2,
	        borderColor:'#bbb',
	        borderRadius:5
		});
		ta_ques.addEventListener( 'change', function(e){
			//! 質問を設定
			IDataManager.SetQuestion( e.value );
		});
		view_ques.add( ta_ques );
		win.add( view_ques );
		
		//! ボタン表示ビュー
		var view_button = Ti.UI.createView({
			title:"view_button",
			width:200,
			height:80,
			layout:'horizontal'
		});
		
		//! ボタン 決定
		var button_yes = Ti.UI.createButton({
			title:'おっけー'
		});
		button_yes.addEventListener( 'click', function() {
			//! 次の画面はルーム管理画面
			var win = IFlowManager.ChangeFlow( IFlowManager.FLOW_HOST_ROOMMANAGE );
			IAppManager.OpenCurrentTab( win );
		});
		view_button.add( button_yes );
		var button_no = Ti.UI.createButton({
			title:'やめとく'
		})
		button_no.addEventListener( 'click', function() {
			//! 閉じる
			win.close();
		});
		view_button.add( button_no );
		
		win.add( view_button );
				
		return win;
	}
	
	//! FLOW_HOST_ROOMMANAGE: ホストルーム管理画面
	this.CreateHostRoomManage = function()
	{
		var win = Ti.UI.createWindow({
			title:"ルーム("+ IDataManager.GetRoomName() +")のホスト画面",
			backgroundColor:'white',
			layout:'vertical'
		});
		
		//! Test
		var test_gest = [
			"のぶとら　さん",
			"ゆうこ　ちゃん",
			"しげよし　くん",
			"ともえ　さま"
		];		
		var member = test_gest.length + 1;
		IDataManager.SetMemberCount( member );
		
		var view_header = Ti.UI.createView({
			title:'view_header',
			top:'3%',
			width:300,
			height:50,
		});
		var lab_member = Ti.UI.createLabel({text:"メンバー一覧 ("+ member +"人)"});
		view_header.add( lab_member )
		win.add( view_header );
		var view_names = Ti.UI.createView({
			title:'view_names',
			width:300,
			height:250,
			layout:'vertical'
		});
		var tblview;
		var data = [];
		for( var i=0 ; i<member ; i++ )
		{
			var row = Ti.UI.createTableViewRow({
				left:20,
				index:i
			});
			if( i==0 ) {
				row.title = IDataManager.GetNickName();
			} else {
				row.title = test_gest[ i-1 ];
			}
			
			var button =Ti.UI.createButton({
				title:'X',
				left:'86%',
				top:10,
				width: 30,
				height:30,
				index:i
			}); 
			button.addEventListener( 'click', function() {
				for( var i=0 ; i<data.length ; i++ )
				{
					if( data[i].index == this.index )
					{
						FAG.Console.write( "Delete i="+ i + "myIndex="+ this.index  );
						data.splice( i, 1);
						break;
					}
				}
				//! ここで参加者の削除を行う。
				tblview.setData( data );
				//! メンバー数変動
				IDataManager.SetMemberCount( data.length );
				lab_member.text = "メンバー一覧 ("+ data.length +"人)";
			});
			row.add( button );
			data.push( row );
			//view_names.add( lab );
		}
		tblview = Ti.UI.createTableView({
			backgroundColor:'gray',
			data:data
		});
		view_names.add( tblview );
		win.add( view_names );
		
		//! ホスト操作ビュー
		var view_hostcontrol = Ti.UI.createView({
			title:'view_hostcontrol',
			width:'auto',
			height:200,
			layout:'vertical',
		});
		
		var buttonStart = Ti.UI.createButton({
			title:'始める'
		});
		
		view_hostcontrol.add(　buttonStart );

		var view_hostanswer = IRoom.CreateJudgeView();
		view_hostanswer.visible = false;
		view_hostcontrol.add( view_hostanswer );
		
		//! 開始ボタンイベント
		buttonStart.addEventListener( 'click', function(){
			//! 開始
			buttonStart.visible = false;
			view_hostanswer.visible = true;
		});		
		
		//! YESボタンイベント
		view_hostanswer.addEventListener( 'onPushTrue', function(){
			IRoom.OpenDialogYesNo("あなたの選択はYESです。 ファイナルアンサー？", function(){
				//! YESの処理
			});
		});
		//! NOボタンイベント
		view_hostanswer.addEventListener( 'onPushFalse', function(){
			IRoom.OpenDialogYesNo("あなたの選択はNOです。 ファイナルアンサー？", function(){
				//! NOの処理
			});			
		});
		
		win.add( view_hostcontrol );
		
		return win;
	}
}
module.exports = RoomManager;

//! インスタンスを取得
var Instance = null;
function GetInstance()
{
	if( Instance == null )
	{
		Instance = new RoomManager();
	}
	return Instance;
}
module.exports.GetInstance = GetInstance; 