/**
 * @author Asaichi-Note00
 * 
 * ルーム関係の汎用処理を行うクラス
 */
function Room()
{
	//! なんかある？
	var FAG = require('fag/FAG');			//! FAGライブラリ
	var DataManager = require('data/DataManager').GetInstance(); 
	
	//! ○×の判定ビューを作成
	//! ○を押した場合 → viewの'onPushTrue' イベントを発行
	//! xを押した場合→ viewの'onPushFalse' イベントを発行
	this.CreateJudgeView = function()
	{
		var viewWidth = 500;
		var viewHeight = 80;
		var view_root = Ti.UI.createView({
			title:"view_root",
			width:viewWidth,
			height:viewHeight
		});
		
		var buttonWidth = 80;
		//! 左ボタン ○
		var leftbutton = Ti.UI.createButton({
			title:'O',
			left:buttonWidth/2,
			width:buttonWidth,
			height:viewHeight
		});
		leftbutton.addEventListener( 'click', function(){
			//! ○ボタンイベントを発行
			view_root.fireEvent( 'onPushTrue' );
		});
		view_root.add( leftbutton );
		
		//! 右ボタン X
		var rightbutton = Ti.UI.createButton({
			title:'X',
			left:viewWidth-buttonWidth - buttonWidth/2,
			width:buttonWidth,
			height:viewHeight
		});
		rightbutton.addEventListener( 'click', function(){
			//! Xボタンイベントを発行
			view_root.fireEvent( 'onPushFalse' );
		});
		view_root.add( rightbutton );
		
		//! 中央に質問
		var label = Ti.UI.createLabel({
			text:DataManager.GetQuestion(),
			width:(viewWidth-buttonWidth*2 - 40),
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		
		view_root.add( label );
		
		return view_root;
	}
	
	//! 汎用のYESNOダイアログを表示
	this.OpenDialogYesNo = function(text,yesevent)
	{
		// ダイアログの生成
		var dialog = Titanium.UI.createAlertDialog({
		    title: 'もういちど確認',
		    message: text,
		    buttonNames: ['いいよ！','まって'],
		    // キャンセルボタンがある場合、何番目(0オリジン)のボタンなのかを指定できます。
		    cancel: 1
		});
		
		// ボタン選択時の処理はイベントハンドラを記述します。
		// 第一引数のindexプロパティで選択されたボタンのindexが設定されます。
		dialog.addEventListener('click',function(event){
		    if( event.index == 0 ){
		        //! リセット時のイベント処理
		        yesevent();
		    }
		    // キャンセル時はevent.cancel == trueとなる
		});
		
		// ダイアログを表示します。
		dialog.show();		
	}
}
module.exports = Room;


//! インスタンスを取得
var Instance = null;
function GetInstance()
{
	if( Instance == null )
	{
		Instance = new Room();
	}
	return Instance;
}
module.exports.GetInstance = GetInstance; 
