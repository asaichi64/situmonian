/**
 * @author Asaichi-Note00
 * 
 * タブの登録と展開を行う。
 */
function Top()
{
	var FAG = require( 'fag/FAG' );
	var IAppManager = require( 'data/AppManager' ).GetInstance();
	var IFlowManager = require( 'data/FlowManager' ).GetInstance();
	
	var myTabGroup = Ti.UI.createTabGroup();
	
//! TAB1 ルーム作成
	//var RoomCreate = require( 'ui/room/RoomCreate' );
	//var IRoomCreate = new RoomCreate(null);
	var tab1 = Titanium.UI.createTab({
	    // icon:'images/tool_0.png',
	    title:"ルーム作成",
	     window:IFlowManager.CreateWindow( IFlowManager.FLOW_HOST_TOP )
	});
	//IRoomCreate.setCurrentTab( tab1 );
	
//! TAB2 ルーム参加
	//
	// create controls tab and root window
	var tab2 = Titanium.UI.createTab({
	    // icon:'images/tool_0.png',
	    title:"ルーム参加",
	     window:IFlowManager.CreateWindow( IFlowManager.FLOW_HOST_TOP )
	});
	//IRoomJoin.setCurrentTab( tab1 );

	//
	//  add tabs
	//
	myTabGroup.addTab(tab1);  
	myTabGroup.addTab(tab2);  	
	
	//! タブを登録
	myTabGroup.addEventListener( 'focus', function(e){
		FAG.Console.write( "FocusTab index=" + e.index );
		
		//! タブを登録
		IAppManager.SetCurrentTab( e.tab ); 
	});
	
	//! 展開
	this.open = function()
	{
		myTabGroup.open();
	}
}
module.exports = Top;